# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

KOTATOGRAM_PN="${PN/-bin/}"

inherit xdg-utils desktop

DESCRIPTION="Unofficial client for Telegram desktop"
HOMEPAGE="https://kotatogram.github.io/ru/"
SRC_URI="https://github.com/kotatogram/kotatogram-desktop/releases/download/k${PV}/${PV}-linux.tar.xz -> kt${PV}.tar.xz"

LICENSE="MPL-2.0"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="gnome-base/gconf:2"
RDEPEND="
	sys-libs/glibc
	dev-libs/glib:2
	>=media-libs/fontconfig-2.13
	media-libs/freetype:2
	virtual/opengl
	x11-libs/libX11
	>=x11-libs/libxcb-1.10[xkb]
"

QA_PREBUILT="*"

S=${WORKDIR}

src_prepare() {
    default
}

src_install() {
    declare KOTATOGRAM_HOME=/opt/${KOTATOGRAM_PN}

    dodir ${KOTATOGRAM_HOME%/*}

    insinto ${KOTATOGRAM_HOME}
        doins -r *

    exeinto ${KOTATOGRAM_HOME}
        doexe Kotatogram

	fperms +x ${KOTATOGRAM_HOME}/Kotatogram/Kotatogram

    dosym ${KOTATOGRAM_HOME}/Kotatogram/Kotatogram /usr/bin/${PN} || die

    # install-xattr doesnt approve using domenu or doins from FILESDIR
}
