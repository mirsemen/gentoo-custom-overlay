# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
LLVM_MAX_SLOT=16
PLOCALES="cs da de fr ja pl ru sl uk zh-CN zh-TW"

inherit llvm cmake flag-o-matic virtualx xdg

DESCRIPTION="Lightweight IDE for C++/QML development centering around Qt"
HOMEPAGE="https://doc.qt.io/qtcreator/"
LICENSE="GPL-3"
SLOT="0"

if [[ ${PV} == *9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://code.qt.io/${PN}/${PN}.git"
else
	MY_PV=${PV/_/-}
	MY_P=${PN}-opensource-src-${MY_PV}
	[[ ${MY_PV} == ${PV} ]] && MY_REL=official || MY_REL=development
	SRC_URI="https://download.qt.io/${MY_REL}_releases/${PN/-}/$(ver_cut 1-2)/${MY_PV}/${MY_P}.tar.xz"
	KEYWORDS="~amd64 ~x86"
	S=${WORKDIR}/${MY_P}
fi

# TODO: unbundle sqlite

QTC_PLUGINS=(android autotest autotools:autotoolsprojectmanager baremetal bazaar +beautifier boot2qt
	'+clangtools:clangcodemodel|clangformat|clangtools' clearcase +cmake:cmakeprojectmanager +cppcheck
	ctfvisualizer cvs +designer +git glsl:glsleditor +help lsp:languageclient mcu:mcusupport mercurial
	modeling:modeleditor meson:mesonprojectmanager cmake:cmakeprojectmanager nim perforce perfprofiler
	python qbs:qbsprojectmanager qmldesigner qmljs:qmljseditor qmlprofiler qnx +remotelinux
	scxml:scxmleditor serialterminal silversearcher +subversion +valgrind webassembly)
IUSE="doc systemd gmldesigner webengine ${QTC_PLUGINS[@]%:*}"
RESTRICT="!test? ( test )"
REQUIRED_USE="
	boot2qt? ( remotelinux )
	clangtools? ( lsp test? ( qbs ) )
	mcu? ( cmake )
	meson? ( designer )
	cmake? ( designer )
	qmldesigner? ( qmljs )
	python? ( lsp )
	qnx? ( remotelinux )
"

# minimum Qt version required
QT_PV="5.14:5"

BDEPEND="
	>=dev-qt/linguist-tools-${QT_PV}
	virtual/pkgconfig
	doc? ( >=dev-qt/qdoc-${QT_PV} )
"
CDEPEND="
	>=dev-qt/qtconcurrent-${QT_PV}
	>=dev-qt/qtcore-${QT_PV}
	>=dev-qt/qtdeclarative-${QT_PV}[widgets]
	>=dev-qt/qtgui-${QT_PV}
	>=dev-qt/qtnetwork-${QT_PV}[ssl]
	>=dev-qt/qtprintsupport-${QT_PV}
	>=dev-qt/qtquickcontrols-${QT_PV}
	>=dev-qt/qtscript-${QT_PV}
	>=dev-qt/qtsql-${QT_PV}[sqlite]
	>=dev-qt/qtsvg-${QT_PV}
	>=dev-qt/qtwidgets-${QT_PV}
	>=dev-qt/qtx11extras-${QT_PV}
	>=dev-qt/qtxml-${QT_PV}
	clangtools? ( <=sys-devel/clang-$((LLVM_MAX_SLOT+1)):= )
	designer? ( >=dev-qt/designer-${QT_PV} )
	help? (
		>=dev-qt/qthelp-${QT_PV}
		webengine? ( >=dev-qt/qtwebengine-${QT_PV}[widgets] )
	)
	perfprofiler? ( dev-libs/elfutils )
	serialterminal? ( >=dev-qt/qtserialport-${QT_PV} )
	systemd? ( sys-apps/systemd:= )
"
DEPEND="${CDEPEND}
	test? (
		>=dev-qt/qtdeclarative-${QT_PV}[localstorage]
		>=dev-qt/qtquickcontrols2-${QT_PV}
		>=dev-qt/qttest-${QT_PV}
		>=dev-qt/qtxmlpatterns-${QT_PV}[qml]
	)
"
RDEPEND="${CDEPEND}
	dev-debug/gdb[python]
	autotools? ( sys-devel/autoconf )
	>=dev-build/cmake-3.14
	cppcheck? ( dev-util/cppcheck )
	cvs? ( dev-vcs/cvs )
	git? ( dev-vcs/git )
	mercurial? ( dev-vcs/mercurial )
	qbs? ( >=dev-util/qbs-1.15 )
	qmldesigner? ( >=dev-qt/qtquicktimeline-${QT_PV} )
	silversearcher? ( sys-apps/the_silver_searcher )
	subversion? ( dev-vcs/subversion )
	valgrind? ( dev-debug/valgrind )
"

# qt translations must also be installed or qt-creator translations won't be loaded
for x in ${PLOCALES}; do
	IUSE+=" l10n_${x}"
	RDEPEND+=" l10n_${x}? ( >=dev-qt/qttranslations-${QT_PV} )"
done
unset x

llvm_check_deps() {
	has_version -d "sys-devel/clang:${LLVM_SLOT}"
}

pkg_setup() {
	use clangtools && llvm_pkg_setup
}

src_prepare() {
	default

	# disable unwanted plugins
	for plugin in "${QTC_PLUGINS[@]#[+-]}"; do
		if ! use ${plugin%:*}; then
			einfo "Disabling ${plugin%:*} plugin"
			sed -i -re "s/^add_subdirectory\(\<(${plugin#*:})\>\)/#\0/" \
				src/plugins/CMakeLists.txt || die "failed to disable ${plugin%:*} plugin"
		fi
	done
	if ! use cmake; then
		sed -i -e '/incredibuild/d' src/plugins/CMakeLists.txt || die
	fi

	if ! use gmldesigner; then
		sed -i -e '/qmldesigner/d' src/plugins/CMakeLists.txt || die
		sed -i -e '/qmlprojectmanager/d' src/plugins/CMakeLists.txt || die
		sed -i -e '/advanceddockingsystem/d' src/libs/CMakeLists.txt || die
		sed -i -e '/qml2puppet/d' src/tools/CMakeLists.txt || die
		sed -i -e '/qmldesigner/d' tests/auto/qml/CMakeLists.txt || die
	fi
	sed -i -re 's/^add_sub\w*\(\<(ios|updateinfo|winrt)\>/#\0/' src/plugins/CMakeLists.txt || die
	sed -i -re '/(iostools|qtcdebugger|wininterrupt|winrtdebughelper)/d' src/tools/CMakeLists.txt || die

	# avoid building unused support libraries and tools
	if ! use clangtools; then
		sed -i -re '/clangsupport|sqlite|yaml-cpp/d' src/libs/CMakeLists.txt || die
		sed -i -re '/clangbackend/d' src/tools/CMakeLists.txt || die
	fi
	if ! use glsl; then
		sed -i -e '/glsl/d' src/libs/CMakeLists.txt || die
	fi
	if ! use lsp; then
		sed -i -e '/languageserverprotocol/d' src/libs/CMakeLists.txt tests/auto/CMakeLists.txt || die
	fi
	if ! use modeling; then
		sed -i -e '/modelinglib/d' src/libs/CMakeLists.txt || die
	fi
	if ! use perfprofiler; then
		rm -r src/tools/perfparser || die
		if ! use ctfvisualizer && ! use qmlprofiler; then
			sed -i -e '/tracing/d' src/libs/CMakeLists.txt tests/auto/CMakeLists.txt || die
		fi
	fi
	if ! use qmljs; then
		sed -i -e '/qmleditorwidgets/d' src/libs/CMakeLists.txt || die
	fi
	if ! use valgrind; then
		sed -i -e '/valgrindfake/d' src/tools/CMakeLists.txt || die
		sed -i -e '/valgrind/d' tests/auto/CMakeLists.txt || die
	fi

	# disable broken or unreliable tests
	sed -i -re '/(manual|tools|unit)/d' tests/CMakeLists.txt || die
	sed -i -e '/^option(WITH_DEBUGGER_DUMPERS/s/ON/OFF/' tests/auto/debugger/CMakeLists.txt || die
	sed -i -re '/\<check\>/d' tests/auto/qml/codemodel/CMakeLists.txt || die
	sed -i -re '/filesystemaccess_test/d' src/plugins/remotelinux/CMakeLists.txt || die

	# fix translations
	local lang languages=
	for lang in ${PLOCALES}; do
		use l10n_${lang} && languages+=" ${lang/-/_}"
	done
	sed -i -re "s/^(set\(languages\s*)(.*)\)/\1${languages})/" share/qtcreator/translations/CMakeLists.txt || die

	# remove bundled syntax-highlighting
	# rm -r src/libs/3rdparty/syntax-highlighting || die

	# remove bundled yaml-cpp
	# rm -r src/libs/3rdparty/yaml-cpp || die

	# remove bundled qbs
	# rm -r src/shared/qbs || die

	sed -i -e "/^set(IDE_QT_VERSION_MIN/s/\".*\"/\"5.15.0\"/" cmake/QtCreatorAPI.cmake || die

	# eapply "${FILESDIR}/terminal.patch"

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		$(use clangtools && echo -DLLVM_INSTALL_DIR="$(get_llvm_prefix ${LLVM_MAX_SLOT})")
		$(use qbs && echo -DQBS_INSTALL_DIR="${EPREFIX}/usr")
		$(use test && echo -DBUILD_TESTS=1)
		#$(use systemd && echo CONFIG+=journald) \
		-DBUILD_WITH_PCH=Off
	)
	cmake_src_configure
}

src_test() {
	cd tests/auto && virtx default
}

src_install() {

	dodoc dist/known-issues

	# install documentation
	if use doc; then
		emake docs
		# don't use ${PF} or the doc will not be found
		insinto /usr/share/doc/qtcreator
		doins share/doc/qtcreator/qtcreator{,-dev}.qch
		docompress -x /usr/share/doc/qtcreator/qtcreator{,-dev}.qch
	fi
	cmake_src_install
}
